using System;
using BasicRabbitMQ.Queueing.Interfaces;

namespace BasicRabbitMQ.Api.Models;

public class WordQueueMessage : IQueueMessage
{
    public Guid MessageId { get; set; }
    public TimeSpan TimeToLive { get; set; }
    
    public string Word { get; set; }
}