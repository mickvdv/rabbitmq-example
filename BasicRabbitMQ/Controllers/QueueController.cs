using System;
using BasicRabbitMQ.Api.Models;
using BasicRabbitMQ.Queueing.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BasicRabbitMQ.Api.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class QueueController : ControllerBase
{
    private readonly IQueueProducer<SentenceQueueMessage> _queueProducer;

    public QueueController(IQueueProducer<SentenceQueueMessage> queueProducer)
    {
        _queueProducer = queueProducer;
    }

    /// <summary>
    /// Places a DemoQueueMessage on the queue 
    /// </summary>
    /// <param name="fail">Indicates if the processing of this message fail, thereby triggering a Deadlettering of the message</param>
    /// <param name="timeToLive">TimeToLive of the created queue message, in milliseconds</param>
    /// <param name="numberOfSecondMessages">Number of messages that will be created in the second step of processing</param>
    /// <returns></returns>
    [HttpGet()]
    public ActionResult SplitSentence(string sentence)
    {
        var message = new SentenceQueueMessage
        {
            TimeToLive = TimeSpan.FromMinutes(1),
            Sentence = sentence
        };

        _queueProducer.PublishMessage(message);

        return Ok($"Message enqueued");
    }
}