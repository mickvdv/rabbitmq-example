using System.Threading.Tasks;
using BasicRabbitMQ.Api.Models;
using BasicRabbitMQ.Queueing.Interfaces;
using Microsoft.Extensions.Logging;

namespace BasicRabbitMQ.Api.Consumers;

public class WordQueueMessageConsumer : IQueueConsumer<WordQueueMessage>
{
    private readonly ILogger<WordQueueMessageConsumer> _logger;

    public WordQueueMessageConsumer(ILogger<WordQueueMessageConsumer> logger)
    {
        _logger = logger;
    }
    public Task ConsumeAsync(WordQueueMessage message)
    {
        _logger.LogInformation($"Word: {message.Word}");

        return Task.CompletedTask;
    }
}