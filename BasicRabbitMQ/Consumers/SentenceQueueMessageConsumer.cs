using System.Linq;
using System.Threading.Tasks;
using BasicRabbitMQ.Api.Models;
using BasicRabbitMQ.Queueing.Exceptions;
using BasicRabbitMQ.Queueing.Interfaces;

namespace BasicRabbitMQ.Api.Consumers;

public class SentenceQueueMessageConsumer : IQueueConsumer<SentenceQueueMessage>
{
    private readonly IQueueProducer<WordQueueMessage> _wordQueueProducer;

    public SentenceQueueMessageConsumer(IQueueProducer<WordQueueMessage> wordQueueProducer)
    {
        _wordQueueProducer = wordQueueProducer;
    }

    public Task ConsumeAsync(SentenceQueueMessage message)
    {
        var words = message.Sentence.Split(" ");

        if (words.Length < 2)
            throw new QueueingException("Sentence should contain multiple words");

        foreach (var word in words)
        {
            var wordMessage = new WordQueueMessage
            {
                Word = word,
                TimeToLive = message.TimeToLive
            };

            _wordQueueProducer.PublishMessage(wordMessage);
        }

        return Task.CompletedTask;
    }
}