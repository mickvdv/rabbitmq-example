﻿using System;

namespace BasicRabbitMQ.Queueing.Interfaces
{
    public interface IQueueMessage
    {
        Guid MessageId { get; set; }
        TimeSpan TimeToLive { get; set; }
    }
}