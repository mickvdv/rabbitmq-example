﻿using RabbitMQ.Client;

namespace BasicRabbitMQ.Queueing.Interfaces
{
    internal interface IConnectionProvider
    {
        IConnection GetConnection();
    }
}