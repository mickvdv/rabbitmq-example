﻿using RabbitMQ.Client;

namespace BasicRabbitMQ.Queueing.Interfaces
{
    internal interface IChannelProvider
    {
        IModel GetChannel();
    }
}